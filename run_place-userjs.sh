#!/bin/sh

cd $HOME/.mozilla/firefox

res=$(cat << EOF | /usr/bin/env python3
import configparser
p = configparser.ConfigParser()
p.read('profiles.ini')

for section in p.sections():
  if p.has_option(section, 'Default') and p[section]['Default'] == '1':

    print(p[section]['Path'])
EOF
)

if ! [ -z "$res" ]; then
  cp user.js $res/user.js
fi
