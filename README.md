# My Config Files
This is where configuration files, dotfiles, and various scripts are kept.

## Management
I use [Chezmoi](https://github.com/twpayne/chezmoi).
Frequently used commands are:

`chezmoi add`

`chezmoi edit`

`chezmoi diff`

`chezmoi apply`

`chezmoi cd` from where `git` can be used as normal.


System wide Nix stuff is plain git (trough hard links), as I do not want to run Chezmoi as root.

### Adding Another Machine
`chezmoi init https://gitlab.com/vdot0x23/dotfiles.git`

`chezmoi apply`

### Pull and Apply
This can be done in one step with `chezmoi update`.
