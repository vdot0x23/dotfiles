{ config, pkgs, ... }:
{
  services = {
     xserver = {
      videoDrivers =
      [
        "intel"
      ];
      displayManager = {
        lightdm = {
          enable = true;
        };
      };
      desktopManager = {
        xfce = {
          enable = true;
        };
      };
    };

    gvfs.enable = true;
    blueman.enable = true;
    
    gnome3.gnome-keyring.enable = true;
  };
  
  security.pam.services = {
    lightdm.enableGnomeKeyring = true;
  };

  programs = {
    dconf.enable = true;
    wireshark.package = pkgs.wireshark-qt;
  };
  
  environment = {
    etc = {
      # Crazy hack below, don't look
      "xfce-lock.sh".text = ''
        i3lock-color -i /home/vdot0x23/Pictures/nix-wallpaper-simple-dark-gray.png -e -f --clock -u --timestr="%H:%M" --timesize=24 --timecolor=FFFFFFFF --datestr="%Y-%m-%d" --datesize=24 --datecolor=FFFFFFFF --timepos=1900:45 --time-align 2 --date-align 2
      '';
      "xfce-log-out.sh".text = ''
        xfce4-session-logout -f
      '';
    };
  };



  environment.systemPackages = with pkgs; 
  [
    elementary-xfce-icon-theme
    epiphany # Backup browser
    fractal # Matrix client
    gcolor3 # Color picker
    geany # IDE
    gnome3.gnome-keyring
    gnome3.seahorse
    gnome3.evolution
    greybird # Theme
    adementary-theme
    i3lock-color
    numix-gtk-theme
    numix-icon-theme
    redshift
    numix-icon-theme-circle
    numix-icon-theme-square
    arc-theme
    adapta-gtk-theme
    gnome3.adwaita-icon-theme
    nordic
    nordic-polar
    mate.atril # pdf reader
    mate.pluma # text editor
    networkmanagerapplet
    # Note: xfce4-terminal has drop-down/pop-up feature --drop-down
    polkit_gnome # Superuser prompt
    xfce4-14.parole # video viewer
    xfce4-14.xfce4-taskmanager
    xfce4-14.xfce4-whiskermenu-plugin
    xfce4-14.xfce4-xkb-plugin # Keyboard layout
    aspellDicts.en
    aspellDicts.en-computers
    aspellDicts.da
  ];
}

