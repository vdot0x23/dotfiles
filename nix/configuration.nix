# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:
{

  imports =
    [
      ./hardware-configuration.nix
      ./xfce.nix
      ./overrides.nix
    ];
    
  boot = {
    tmpOnTmpfs = true;
    kernelParams = 
    [ 
      "intel_iommu=on" # For intel
    ];

    loader = {
      timeout = 1;
      systemd-boot = {
        enable = true;
        editor = false;
      };
    };
  };
  
  systemd.extraConfig = ''TimeoutSec=25s'';

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "dk";
    defaultLocale = "en_DK.UTF-8";
  };
  
  time = {
    timeZone = "Europe/Copenhagen";
  };
  
  location.provider = "geoclue2"; # Needed for redshift

  sound = {
    enable = true;
  };
  
  hardware = {
    opengl = {
      driSupport32Bit = true;
      extraPackages = with pkgs; [
        vaapiIntel
        vaapiVdpau
        libvdpau-va-gl
        intel-media-driver
        # intel-compute-runtime
      ];
    };

    pulseaudio = {
      enable = true;
      package = pkgs.pulseaudioFull; # For Bluetooth support
    };
    bluetooth = {
      enable = true;
      package = pkgs.bluezFull;
      powerOnBoot = false;
    };
  };

  system = {
    stateVersion = "19.09"; # Only  change if NixOS release notes explicitly say you so.
    autoUpgrade = {
        enable = true; # Enable automatic upgrades with a systemd timer
    };
  };
  
  nix = {
    autoOptimiseStore = true; # Hard link identical packages
    gc = {
      automatic = true; # Garbage collect regularly
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };
  
  # Define user accounts. Set a password with `# passwd [LOGIN]`.
  users.users = {
    vdot0x23 = {
      isNormalUser = true;
      extraGroups = 
      [ 
        "adbusers"
        "wheel"
        "networkmanager"
        "wireshark"
        "libvirtd"
	"dialout"
      ];
    };
    gameman = {
      isNormalUser = true; # Dedicated user of old/insecure/nonfree games
    };
  };

  networking = {
    extraHosts = ''
      130.226.99.2 ssl-vpn1.aau.dk
      127.0.0.1 nextcloud.fricloud-test.test 
    '';
    hostName = "vicomptwo";
    firewall = {
      enable = true;
      allowedTCPPorts = 
      [
        6112 # Warcraft 3
      ];
      allowedUDPPorts = 
      [
        6112 # Warcraft 3
      ];
    };

    networkmanager = {
      enable = true;
      dns = "dnsmasq";
      dispatcherScripts = 
      [ {
        source = pkgs.writeText "upHook" ''
          if [[ $DEVICE_IFACE == *"wlp"* || $DEVICE_IFACE == *"enp"* ]]; then
            /run/current-system/sw/bin/nmcli connection modify uuid $CONNECTION_UUID \
            ipv4.dhcp-send-hostname "false" \
            ipv4.ignore-auto-dns "true" \
            ipv4.dns "127.0.0.1" \
            ipv6.dhcp-send-hostname "false" \
            ipv6.ignore-auto-dns "true" \
            ipv6.dns "0::1" \
            >> /tmp/dispatcherScripts.log
          else
            /run/current-system/sw/bin/nmcli connection modify uuid $CONNECTION_UUID \
            ipv4.dhcp-send-hostname "false" \
            ipv4.dns "" \
            ipv4.ignore-auto-dns "false" \
            ipv6.dhcp-send-hostname "false" \
            ipv6.ignore-auto-dns "false" \
            ipv6.dns "" \
            echo "Excluding $CONNECTION_ID on $DEVICE_IFACE from DNS dispatcher script" >> /tmp/dispatcherScripts.log
          fi
        '';
        type = "pre-up";
      } ]; # Use local DNS server and don't send hostname. For all connections
    };
  };
  
  environment = {
    etc = {
        "NetworkManager/dnsmasq.d/dnsmasq-nm.conf".text = ''
          no-resolv
          server=127.0.0.1#5453
          server=0::1#5453
          cache-size=5000
        '';
          "NetworkManager/dnsmasq-shared.d/dnsmasq-shared-nm.conf".text = ''
          no-resolv
          server=127.0.0.1#5453
          server=0::1#5453
          cache-size=5000
        '';
    };
    variables = {
      EDITOR = "nvim";
      VISUAL = "nvim";
    };

  };
  
  services = {
    logind = {
      lidSwitch = "ignore";
    };

    gitlab-runner = {
      enable = true;
      configFile = /etc/gitlab-runner/config.toml;
      packages = with pkgs; [ 
        bash
        docker-machine
        (writers.writeDashBin "vboxmanage" ''
          ${virtualbox}/bin/VBoxManage "$@"
        '')
        ];
    };

    stubby = {
      enable = true;
      listenAddresses = 
      [ 
        "127.0.0.1@5453"
        "0::1@5453"
      ];
      upstreamServers = ''
        ## IPv4 ##
        # The Surfnet/Sinodun servers
          - address_data: 145.100.185.15
            tls_auth_name: "dnsovertls.sinodun.com"
            tls_pubkey_pinset:
              - digest: "sha256"
                value: 62lKu9HsDVbyiPenApnc4sfmSYTHOVfFgL3pyB+cBL4=
          - address_data: 145.100.185.16
            tls_auth_name: "dnsovertls1.sinodun.com"
            tls_pubkey_pinset:
              - digest: "sha256"
                value: cE2ecALeE5B+urJhDrJlVFmf38cJLAvqekONvjvpqUA=
        # The getdnsapi.net server
          - address_data: 185.49.141.37
            tls_auth_name: "getdnsapi.net"
            tls_pubkey_pinset:
              - digest: "sha256"
                value: foxZRnIh9gZpWnl+zEiKa0EJ2rdCGroMWm02gaxSc9Q=
        # Foundation for Applied Privacy
          - address_data: 37.252.185.232
            tls_auth_name: "dot1.appliedprivacy.net"
            tls_port: 443 # In case default 853 is blocked
        # digitalcourage
          - address_data: 46.182.19.48
            tls_auth_name: "dns2.digitalcourage.de"
            tls_pubkey_pinset:
              - digest: "sha256"
                value: v7rm6OtQQD3x/wbsdHDZjiDg+utMZvnoX3jq3Vi8tGU=
        # The Uncensored DNS servers
          - address_data: 89.233.43.71
            tls_auth_name: "unicast.censurfridns.dk"
            tls_pubkey_pinset:
              - digest: "sha256"
                value: wikE3jYAA6jQmXYTr/rbHeEPmC78dQwZbQp6WdrseEs=
        ## IPv6 ##
        # The Surfnet/Sinodun servers
          - address_data: 2001:610:1:40ba:145:100:185:15
            tls_auth_name: "dnsovertls.sinodun.com"
            tls_pubkey_pinset:
              - digest: "sha256"
                value: 62lKu9HsDVbyiPenApnc4sfmSYTHOVfFgL3pyB+cBL4=
          - address_data: 2001:610:1:40ba:145:100:185:16
            tls_auth_name: "dnsovertls1.sinodun.com"
            tls_pubkey_pinset:
              - digest: "sha256"
                value: cE2ecALeE5B+urJhDrJlVFmf38cJLAvqekONvjvpqUA=
        # The getdnsapi.net server
          - address_data: 2a04:b900:0:100::38
            tls_auth_name: "getdnsapi.net"
            tls_pubkey_pinset:
              - digest: "sha256"
                value: foxZRnIh9gZpWnl+zEiKa0EJ2rdCGroMWm02gaxSc9Q=
        # Foundation for Applied Privacy
          - address_data: 2a00:63c1:a:229::3
            tls_port: 443 # In case default 853 is blocked
            tls_auth_name: "dot1.appliedprivacy.net"
        # digitalcourage
          - address_data: 2a02:2970:1002::18
            tls_auth_name: "dns2.digitalcourage.de"
            tls_pubkey_pinset:
              - digest: "sha256"
                value: v7rm6OtQQD3x/wbsdHDZjiDg+utMZvnoX3jq3Vi8tGU=
        # The Uncensored DNS server
          - address_data: 2a01:3a0:53:53::0
            tls_auth_name: "unicast.censurfridns.dk"
            tls_pubkey_pinset:
              - digest: "sha256"
                value: wikE3jYAA6jQmXYTr/rbHeEPmC78dQwZbQp6WdrseEs=
      '';
    };
    
    xserver = {
      enable = true;
      layout = "dk";
      xkbOptions = "eurosign:e";
      libinput = {
        enable = true; # Enable touchpad support.
        accelProfile = "flat";
      };
      config = ''
        Section "InputClass"
          Identifier "mouse accel"
          Driver "libinput"
          MatchIsPointer "on"
          Option "AccelProfile" "flat"
          Option "AccelSpeed" "0"
        EndSection
      '';
    };
    
    printing = {
      enable = true; # Enable CUPS to print documents.
    };
    
    tor = {
      enable = true; # "Slow" tor on port 9050, for email, git, etc. 
    };
    
    fwupd = {
      enable = true; # Firmware upgrade
    };
  };
  
  virtualisation = {
    libvirtd = {
      enable = true;
    };
    docker = {
      enable = true;
      liveRestore = false;
    };
    virtualbox.host = {
      enable = true;
    };
  };
  
  programs = { 
    wireshark.enable = true;
    adb.enable = true;
  };

  environment.systemPackages = with pkgs; 
  [
    ansible
    mosh
    arduino
    audacity
    bc
    curl
    dash # Posix-only shell
    dnsutils
    entr
    exfat
    firefox
    freecad
    fzf
    gcc
    gimp
    git
    gnumake
    gnupg
    gotop
    mumble
    go
    gst-plugins-bad
    gst-plugins-base
    gst-plugins-good
    gst-plugins-ugly
    gparted
    htop
    imagemagick
    inkscape
    keepassxc
    kicad
    libreoffice
    neovim
    nmap
    octaveFull
    openscad
    pandoc
    rsync
    texlive.combined.scheme-full
    tikzit # Draw to PGF/TikZ
    qt5.full # Temporary to fix err
    tor-browser-bundle-bin
    traceroute
    tree
    vagrant
    vim
    virtmanager
    wget
    whois
    wineWowPackages.stable
    mono
    woeusb
    neofetch
    tmux
    sshfs
    sslscan
    falkon
    unzip
    tig
    p7zip
    python3
    usbutils
    pciutils
    docker-compose
    openssl
    kdeApplications.kolourpaint
    zsh
    xclip
    chezmoi
    x2goclient
    proxychains
    vlc
    ghc
  ];
}
